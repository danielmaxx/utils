import sys
import os
import getpass
from subprocess import call

def clean_all(final_name, path_to_conf, directory):
  try:
    os.remove(path_to_conf)
  except:
    print "Failed to remove conf file"
  try:
    os.rmdir(directory)
  except:
    print "Failed to remove schroot directory"

def user_choose():

  while True:
    name=raw_input("Choose the name of your environment: ")
    if not name.isalnum():
      print "Choose a valid name: it only should contain alphanumeric characters."
    else:
      break

  while True:
    arch=raw_input("Choose the architecture of your environment [i386/amd64]: ")
    if not (arch!="i386" or arch!="amd64"):
      print "Choose a valid architecture."
    else:
      break

  while True:
    distro = raw_input("Choose the Ubuntu's distribution of your environment: ")
    final_name = distro+"_"+arch+"_"+name
    path = "/etc/schroot/chroot.d/" + final_name + "_conf"
    directory = "/srv/chroot/"+final_name
    if os.path.exists(path) and os.path.exists(directory):
      print "Existing environment name, try another name"
    else:
      break

  return (name, distro, arch, path, final_name, directory)

def prepare_environment():
  (name, distro, arch, path, final_name, directory) = user_choose()
  cur_user = getpass.getuser()
  conf_content= "\n\
# Please edit this file with your user preferences \n\
["+final_name+"]\n\
description=Ubuntu " + name + " for " + arch + "\n\
directory=/srv/chroot/"+final_name+"\n\
#personality=linux32\n\
root-users="+cur_user+"\n\
#run-setup-scripts=true\n\
#run-exec-scripts=true\n\
type=directory\n\
users="+cur_user

  f = open(path, "w")
  f.write(conf_content)
  f.close()

  cmd = "nano"
  call([cmd, path])

  if not os.path.exists(directory):
    os.makedirs(directory)
  else:
    print "Existing directory: {}. Process has failed. Cleaning everything.".format(directory)
    clean_all(final_name, path, directory)
    sys.exit(1)
  
  return (name, distro,  arch, path, final_name, directory)

def create_environment(name, distro, arch, path, final_name, directory):
  args = ["debootstrap", "--variant=buildd", "--arch="+arch, distro,  directory, "http://archive.ubuntu.com/ubuntu/"]
  print args
  try:
    call(args)
  except:
    print "Failed to create environment. Cleaning everything"
    clean_all(final_name, path, directory)
    sys.exit(1)

def install_minimal(name, distro, arch):
  args = ["schroot", " -c " + distro + "_" + arch + "_" + name, " -u root", " -- apt-get install ubuntu-minimal language-pack-en language-pack-es git-core nano -y"]
  f = open("install_minimal"+distro+arch+name+".sh", "w")
  #call(["echo", "\""+"".join(args)+"\" > install_minimal"+distro+arch+name+".sh"])
  #call(args)
  f.write("".join(args))
  f.close()
  call(["sh", "install_minimal"+distro+arch+name+".sh"])

def main():
  (name, distro, arch, path, final_name, directory) = prepare_environment()
  create_environment(name, distro, arch, path, final_name, directory)
  while True:
    is_install_minimal=raw_input("Install minimal ubuntu?[y/n]: ")
    if not (is_install_minimal!="y" or is_install_minimal!="n"):
      print "Choose a valid answer."
    else:
      break
  if is_install_minimal == "y":
    install_minimal(name, distro, arch)
    #install_minimal("mcap", "precise", "i386")
    call(["rm", "install_minimal"+distro+arch+name+".sh"])

if __name__ == "__main__":
    main()


